require 'net/http'
require 'net/ping'
require 'benchmark'
require 'parallel'
#require_relative 'config/environment'

# class AllProxy < ActiveRecord::Base
# 0.5 0.27 0.77 (79.047286)
# end
# class ActiveProxy < ActiveRecord::Base
#   validates :ip, uniqueness: true
# end
# class BadProxy < ActiveRecord::Base
#
# end
# class GrayProxy < ActiveRecord::Base
#
# end
def start_new(i,n)
  puts "#{n} _ #{i} :::: #{Time.now}"

end
arr = (1..10000).to_a
Benchmark.bm do |bm|
  bm.report do
    Parallel.each_with_index(arr, in_threads: 32) do |index,number|
      start_new(index, number)
    end
  end
end