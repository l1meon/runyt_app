require 'net/http'
require 'net/ping'
require 'benchmark'
require 'parallel'
require_relative 'config/environment'

class AllProxy < ActiveRecord::Base

end
class ActiveProxy < ActiveRecord::Base
  validates :ip, uniqueness: true
end
class BadProxy < ActiveRecord::Base
  validates :ip, uniqueness: true
end
class GrayProxy < ActiveRecord::Base
  validates :ip, uniqueness: true
end

def check_all(proxy)
  proxy_addr = proxy.ip
  proxy_port = proxy.port.to_i
  #uri = URI.parse('http://185.125.219.73:3000/home/index.json/')
  @proxy = Net::Ping::TCP.new(proxy_addr, proxy_port.to_i)
  if @proxy.ping? && @proxy.duration && @proxy.duration < 1.9
    puts @proxy.duration < 1.9 if @proxy.duration
    begin
      p "pinnnnng"
      @request = Net::HTTP::Get.new('/', {'User-Agent' => 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/42.0.2311.90 Safari/537.360', 'Content-Type' => 'application/json', 'Accept' => 'application/json'})
      Net::HTTP.new('185.125.219.73:3000/home/index.json',nil,proxy_addr, proxy_port).start do |http|
        @res = http.request @request
        p @res.code.is_a? String
        if @res.code == '200' || @res.code == 200
          puts "GET ACTION CODE: #{@res.code}"
          parsed = JSON.parse(@res.body)
          parsed.each do |key, value|
            p "#{key} :: #{value}"
            if value && value.include?('188.138.128.47')
              puts "Bad Proxy"
              BadProxy.create!(ip: proxy_addr, port: proxy_port.to_i)#, all_proxy_id: proxy.id)
            elsif parsed && !parsed['VIA'].nil? || !parsed['HTTP_VIA'].nil?
              puts "Gray proxy"
              GrayProxy.create!(ip: proxy_addr, port: proxy_port.to_i)#, all_proxy_id: proxy.id)
              @working += 1
              sps = proxy_addr.to_sym
              @wips[sps] = proxy_port
            else
              puts "Elite Proxy"
              ActiveProxy.create!(ip: proxy_addr, port: proxy_port.to_i)#, all_proxy_id: proxy.id)
              sps = proxy_addr.to_sym
              @wips[sps] = proxy_port
              @working += 1
            end
          end
        else
          BadProxy.create!(ip: proxy_addr, port: proxy_port.to_i)
          ActiveProxy.delete(proxy)
          puts 'Dead proxy -- bad response code'
        end
      end
    rescue Exception => error
      puts error
      #next
    end
  else
    BadProxy.create!(ip: proxy_addr, port: proxy_port.to_i)
    ActiveProxy.delete(proxy)
    puts 'Dead proxy -- Not ping'
  end
end
active_proxies = ActiveProxy.first(100)
Benchmark.bm do |bm|
  bm.report do
    Parallel.each(active_proxies, in_threads: 32) do |proxy|
      begin
        # p "#{proxy['ip'].strip} : #{proxy['port']}"
        check_all(proxy)
      rescue Exception => error
        puts error
        next
      end
      #proxy_addr = '108.165.33.6'
      #proxy_port = 3128
    end
  end
end
puts @working
puts @wips

#0.34 0.07 0.41 43.84