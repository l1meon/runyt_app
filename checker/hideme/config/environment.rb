require 'rubygems'
require 'active_record'
require 'pg'
require 'yaml'

database_config = YAML::load(File.open(File.join(File.dirname(__FILE__), 'database.yml')))

ActiveRecord::Base.logger = Logger.new(STDERR)
ActiveRecord::Base.establish_connection(database_config)
