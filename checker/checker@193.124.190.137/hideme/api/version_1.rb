require 'rubygems'
require 'net/http'
require 'net/ping'
require 'json'
require_relative '../config/environment'
require_relative '../models/all_proxy'
require_relative '../models/active_proxy'
require_relative '../models/bad_proxy'
require_relative '../models/gray_proxy'
module Api
  class Version1

    API_KEY = '283478000480681'
    API_URL = 'http://hideme.ru/api/checker.php?'

    def initialize
      @uri = URI(API_URL)
      @http = Net::HTTP.new(@uri.host, @uri.port)
    end

    def start_check
      proxy_checked = 0
      #proxies = AllProxy.last(10000).map{|proxy| "#{proxy.ip}:#{proxy.port}"}
      at_index = 0
      begin
        proxies = AllProxy.find_each(start: at_index, batch_size: 3000).first(3000).map{|proxy| "#{proxy.ip}:#{proxy.port}"}
        upload_list(proxies)
        if check_status
          get_list
        end
        proxies.shift(3000) if proxies.count > 0
        @proxies_array.shift(3000) if @proxies_array.count > 0
        proxy_checked += 3000
        at_index += 3000
      end until proxy_checked == 3000 #@proxies_array == 0 #
    end

    def upload_list(array)
      @ips_data = {}
      request = Net::HTTP::Post.new(@uri.request_uri)
      @proxies_array = array
      @ips_string = ""
      @proxies_array.first(3000).each{|proxy| @ips_string << "#{proxy}\n"}
      request.set_form_data('key' => API_KEY.to_i, 'out' => 'js', 'action' => 'list_new', 'tasks' => 'http,ssl,socks4,socks5', 'parser' => 'lines', 'data' => @ips_string)
      response = @http.request(request)
      @upload_response = response.body
      get_group = response.body.scan(/\d+/)
      @proxy_ids = response.body.scan(/(?:,\d+|{\d+)/)
      @proxy_ids.map!{|proxy| proxy.gsub!('{','') || proxy.gsub!(',','') }
      @proxy_ids.each{|proxy| @upload_response.gsub!("#{proxy}", "#{proxy.to_json}")}
      parsed_response = JSON.parse(@upload_response)
      parsed_response['items'].each{|key, value| @ips_data[key] = {ip: value['host'], port: value['port']}}
      @group_id = get_group.last
    end

    def get_list
      @get_url = Net::HTTP::Get.new("http://hideme.ru/api/checker.php?key=#{API_KEY.to_i}&out=js&action=get&groups=#{@group_id.to_i}&filters=progress!:queued;changed:1&fields=progress_simple,resolved_ip,progress_http,progress_ssl,progress_socks4,progress_socks5,time_http,time_socks4,time_socks5,time_ssl,result_http,result_ssl,result_socks4,result_socks5")
      response = @http.request(@get_url)
      response_body = response.body
      @proxy_ids.each do |proxy|
        response_body.gsub!("#{proxy}","#{proxy.to_json}")
      end
      parsed_response = JSON.parse(response_body)
      @working = 0
      parsed_response.each do |array, hash|
        if hash.is_a? Hash
          hash.each do |key, value|
            value.each do
                if value['result_http']['status'] == 1 || value['result_ssl']['status'] == 1 || value['result_socks4']['status'] == 1 || value['result_socks5'] == 1# socks_status
                  if value['result_http']['details'].nil? || value['result_ssl']['details'].nil? || value['result_socks4']['details'].nil? || value['result_socks5']['details'].nil?
                    ActiveProxy.create!(ip: @ips_data[key][:ip], port: @ips_data[key][:port])
                  else
                    GrayProxy.create!(ip: @ips_data[key][:ip], port: @ips_data[key][:port])
                  end
                  # ActiveProxy.create!(ip: @ips_data[key][:ip], port: @ips_data[key][:port])
                  puts "#{@ips_data[key][:ip]}:#{@ips_data[key][:port]} ------working"
                  @working += 1
                else # elsif status == 1 && !details.nil? GrayProxy.create!(ip: @ips_data[key][:ip], port: @ips_data[key][:port])
                  BadProxy.create!(ip: @ips_data[key][:ip], port: @ips_data[key][:port])
                  puts "PROXY::BAD :("
                end
            end
          end
        else
          puts "#{array} : #{hash}"
        end
      end
      puts @working
    end

    def check_status
      @finished = false
      #true if parsed_response['finished'] == 3
      begin
        @status_url = Net::HTTP::Get.new("http://hideme.ru/api/checker.php?key=#{API_KEY}&out=js&action=stat&groups=#{@group_id.to_i}")
        response = @http.request(@status_url)
        parsed_response = JSON.parse(response.body)
        sleep(5)
        system('clear')
        puts parsed_response['finished']
        #puts parsed_response
        if parsed_response['finished'] == '3000' || parsed_response['finished'] == 3000
          @finished = true
        #elsif parsed_response['finished'] <= 5 && parsed_response['finished'] > 4
        # @finished = true
        #  puts parsed_response
        else
          print "Count \n"
        end
      rescue Exception => error
        puts error
      end until @finished == true
      p @finished
      @finished
    end

  end
end

checker_1 = Api::Version1.new
checker_1.start_check
#puts AllProxy.first(3000).map{|p| "#{p.ip}:#{p.port}"}