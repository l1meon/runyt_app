<?
/*
Because XMLHttpRequest does not support cross-domain requests, we need a backend proxy.
This also allows you to attach your key to all requests, without writing it in public code (html/js).
Note, that it's only demonstration code. It doesn't block requests from other sites, and may also have other security vulnerabilities
*/

$key=""; // Leave empty for demo

header("Content-Type: text/html; charset=windows-1251");
header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT"); 
header("Cache-Control: no-store, no-cache, must-revalidate"); 
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");

/*$ip="188.40.74.43";*/
$ip='188.138.128.47';
$url="/api/checker.php?";

foreach($_GET as $k=>$v){
	$url.="{$k}=".rawurlencode($v)."&";
}
if(!empty($key)){
	$url.="key={$key}";
}

$f=fsockopen($ip, 80);
if(isset($_POST['data'])){
	$post="data=".rawurlencode($_POST['data']);
	fwrite($f, "POST {$url} HTTP/1.0\r\nHost: hideme.ru\r\nConnection: Close\r\nContent-Type: application/x-www-form-urlencoded\r\nContent-Transfer-Encoding: binary\r\nContent-Length: ".strlen($post)."\r\n\r\n{$post}");

}else{
	fwrite($f, "GET {$url} HTTP/1.0\r\nHost: hideme.ru\r\nConnection: Close\r\n\r\n");
}
$tmp=stream_get_contents($f);
echo substr($tmp, strpos($tmp, "\r\n\r\n")+4);

?>