class CreateVideosTable < ActiveRecord::Migration
  def self.up
    create_table :videos do |t|
      t.string :title
      t.string :url
      t.integer :views_count
    end
  end

  def self.down
    drop_table :videos
  end
end
