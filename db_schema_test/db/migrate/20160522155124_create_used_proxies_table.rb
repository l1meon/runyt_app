class CreateUsedProxiesTable < ActiveRecord::Migration
  def self.up
    create_table :used_proxies do |t|
      t.references :proxy, index: true, foreign_key: true
      t.references :video, index: true, foreign_key: true
      t.datetime :session_started
      t.datetime :session_end, default: Time.now
      t.integer  :used_times
    end
  end

  def self.down
    drop_table :used_proxies
  end
end
