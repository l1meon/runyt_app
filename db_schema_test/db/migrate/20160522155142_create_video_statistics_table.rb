class CreateVideoStatisticsTable < ActiveRecord::Migration
  def self.up
    create_table :video_statistics do |t|
      t.references :video, index: true, foreign_key: true
      t.references :proxy, index: true, foreign_key: true
      t.datetime :started_at
      t.integer :views_before
      t.datetime :finished_at, default: Time.now
      t.integer :views_after
      t.text :headers_response
    end
  end

  def self.down
  end
end
