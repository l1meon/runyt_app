class CreateProxiesTable < ActiveRecord::Migration
  def self.up
    create_table :proxies do |t|
      t.string :ip
      t.integer :port
      t.datetime :last_check, default: Time.now
      t.string :status_before_check
      t.string :status_after_check
    end
  end

  def self.down
    drop_table :proxies
  end
end
