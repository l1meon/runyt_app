require 'rubygems'
require 'active_record'
require 'sqlite3'
require 'yaml'

database_config = YAML::load(File.open(File.join(File.dirname(__FILE__), 'database.yml')))

ActiveRecord::Base.logger = Logger.new(STDERR)
ActiveRecord::Base.establish_connection(database_config)
#connected = ActiveRecord::Base.connection_pool.with_connection { |con| con.active? }
#puts ActiveRecord::Base.connected?
