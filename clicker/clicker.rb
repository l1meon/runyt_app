require 'rubygems'
require 'capybara'
require 'capybara/webkit'
require 'capybara/dsl'
module Capy
  def self.configure
    Capybara.default_max_wait_time = 10
    Capybara.register_driver :webkit do |app|
      user_agent = { agent: 'HTTP_USER_AGENT'}
      Capybara::Webkit::Driver.new(app,
        :headers => { 'HTTP_USER_AGENT' => 'Mozilla/5.0 (X11; Ubuntu; Linux i686; rv:43.0) Gecko/20100101 Firefox/43.0'}
      #:proxy => { host:'217.106.65.253', port: 3128 }
      )
    end

    Capybara::Webkit.configure do |config|
      config.debug = false
      config.allow_unknown_urls
    end

    Capybara.run_server = false
    Capybara.javascript_driver = :webkit
    Capybara.current_driver = :webkit
  end
end

module Clicker
  class Ads
    include Capybara::DSL
    include Capy

    Capy.configure

    def runyt
      # visit('https://www.youtube.com/watch?v=SMqEhhVjQ74')
      # puts response_headers
      # sleep(20)
      # print "/////////////////////////////////////\n"
      # puts response_headers
      visit('http://ifconfig.co')
      puts find('.ip').text
      if has_css?('.ip')
        puts "IT HAS IP"
      end
      if status_code == 200
        print "STATUS OK"
      end
      print "////////////////\n"
      puts response_headers

    end

    def ww
      w1 = open_new_window
      switch_to_window(w1)
      c1 = current_window
      w2 = open_new_window
      puts windows.count
      switch_to_window(w2)
      c2 = current_window
      puts "Same window: #{c1 == c2}"
      puts windows.count
      within_window c2 do
        c1.close
      end
      #c1.close
      puts windows.count
    end

    def testing
      visit('https://www.youtube.com/watch?v=Zn224sbihNY')
      20.times{print '//'}
      print "\n"
      puts windows
      puts current_url
      puts windows.count
      20.times{print '//'}
      print "\n"
      upl = find('#upload-btn')
      @hrf = upl[:href].to_s
      puts upl[:href]
      win2 = open_new_window
      switch_to_window(win2)
      puts windows.count
      puts windows
      puts current_window
      puts "We visit #{@hrf}"
      visit('http://ifconfig.co')
      #click_on('upload-btn')
      # @up_win = window_opened_by{click_on('upload-btn')}
      # switch_to_window(@up_win)
      puts windows.count
    end

    def get_site
      visit('https://www.youtube.com/watch?v=Zn224sbihNY')
      @main_window =  current_window
      10.times{print "//"}
      puts @main_window
      10.times{print "//"}
      sleep(15)
      unless @main_window.is_a?(String)
        within_window @main_window do
          20.times{print '*'}
          print "\n"
          puts current_url
          20.times{print '*'}
          print "\n"
          sleep(5)
          @upload_window = window_opened_by{click_link_or_button('Upload')}
          sleep(5)
          within_window @upload_window do
            print "***************************************\n"
            puts current_url
            print "************************************\n"
          end
          sleep(5)
          puts current_url
          # if has_css?('a#aw0')
          #   @ads_window = window_opened_by { click_link('#aw0') }
          #   within_window @ads_window do
          #     puts "***********************************************"
          #     puts current_url
          #     puts "***********************************************"
          #     sleep(30)
          #   end
          # end
        end
      end
      puts current_url
      20.times{print '*'}
      print "\n"
      puts windows
      print "\n"
      20.times{print '*'}
    end

    def click
      has = has_css?('#aw0')
      if has
        click_link('#aw0')
      end
      puts current_url
    end
  end
end
c1 = Clicker::Ads.new
c1.runyt
#c1.testing
# within_window new_w do
#
# end